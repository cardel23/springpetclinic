package com.cubisoft.petclinic.jpa;

import com.cubisoft.petclinic.model.Owner;
import com.cubisoft.petclinic.repo.OwnerRepo;
import com.cubisoft.petclinic.repo.PetRepo;
import com.cubisoft.petclinic.repo.PetTypeRepo;
import com.cubisoft.petclinic.service.OwnerService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */
@Service
@Profile("jpa")
public class OwnerJpa implements OwnerService {

    private final OwnerRepo ownerRepo;
    private final PetRepo petRepo;
    private final PetTypeRepo petTypeRepo;

    public OwnerJpa(OwnerRepo ownerRepo, PetRepo petRepo, PetTypeRepo petTypeRepo) {
        this.ownerRepo = ownerRepo;
        this.petRepo = petRepo;
        this.petTypeRepo = petTypeRepo;
    }


    @Override
    public Owner findByLastName(String lastName) {
        return ownerRepo.findByLastName(lastName);
    }

    @Override
    public Owner findById(Long id) {
        return ownerRepo.findById(id).orElse(null);
    }

    @Override
    public Owner save(Owner entity) {
        System.out.println("$$$$$$$");
        return ownerRepo.save(entity);
    }

    @Override
    public Set<Owner> findAll() {
        Set<Owner> owners = new HashSet<>();
        ownerRepo.findAll().forEach(owners::add);
        return owners;
    }

    @Override
    public void delete(Owner entity) {
        ownerRepo.delete(entity);
    }

    @Override
    public void deleteById(Long id) {
        ownerRepo.deleteById(id);
    }

    @Override
    public List<Owner> findByLastNameLike(String lastName) {
        return ownerRepo.findByLastNameLike(lastName);
    }
}
