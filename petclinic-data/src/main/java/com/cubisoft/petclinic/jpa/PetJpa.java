package com.cubisoft.petclinic.jpa;

import com.cubisoft.petclinic.model.Pet;
import com.cubisoft.petclinic.repo.PetRepo;
import com.cubisoft.petclinic.service.PetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */

@Service
@Profile("jpa")
public class PetJpa implements PetService {

    private final PetRepo petRepo;

    public PetJpa(PetRepo petRepo) {
        this.petRepo = petRepo;
    }

    @Override
    public Pet findById(Long id) {
        return petRepo.findById(id).orElse(null);
    }

    @Override
    public Pet save(Pet pet) {
        return petRepo.save(pet);
    }

    @Override
    public Set<Pet> findAll() {
        Set<Pet> pets = new HashSet<>();
        petRepo.findAll().forEach(pets::add);
        return pets;
    }

    @Override
    public void delete(Pet entity) {
        petRepo.delete(entity);
    }

    @Override
    public void deleteById(Long id) {
        petRepo.deleteById(id);
    }
}
