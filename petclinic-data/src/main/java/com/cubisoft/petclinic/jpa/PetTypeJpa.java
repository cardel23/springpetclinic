package com.cubisoft.petclinic.jpa;

import com.cubisoft.petclinic.model.PetType;
import com.cubisoft.petclinic.repo.PetTypeRepo;
import com.cubisoft.petclinic.service.PetTypeService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */
@Service
@Profile("jpa")
public class PetTypeJpa implements PetTypeService {

    private final PetTypeRepo petTypeRepo;

    public PetTypeJpa(PetTypeRepo petTypeRepo) {
        this.petTypeRepo = petTypeRepo;
    }

    @Override
    public PetType findById(Long aLong) {
        return petTypeRepo.findById(aLong).orElse(null);
    }

    @Override
    public PetType save(PetType entity) {
        return petTypeRepo.save(entity);
    }

    @Override
    public Set<PetType> findAll() {
        Set<PetType> petTypes = new HashSet<>();
        petTypeRepo.findAll().forEach(petTypes::add);
        return petTypes;
    }

    @Override
    public void delete(PetType entity) {
        petTypeRepo.delete(entity);
    }

    @Override
    public void deleteById(Long aLong) {
        petTypeRepo.deleteById(aLong);
    }
}
