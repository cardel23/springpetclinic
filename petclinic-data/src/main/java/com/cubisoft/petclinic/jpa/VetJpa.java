package com.cubisoft.petclinic.jpa;

import com.cubisoft.petclinic.model.Vet;
import com.cubisoft.petclinic.repo.VetRepo;
import com.cubisoft.petclinic.service.VetService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */
@Service
@Profile("jpa")
public class VetJpa implements VetService {

    private final VetRepo vetRepo;

    public VetJpa(VetRepo vetRepo) {
        this.vetRepo = vetRepo;
    }

    @Override
    public Vet findById(Long id) {
        return vetRepo.findById(id).orElse(null);
    }

    @Override
    public Vet save(Vet entity) {
        return vetRepo.save(entity);
    }

    @Override
    public Set<Vet> findAll() {
        Set<Vet> vets = new HashSet<>();
        vetRepo.findAll().forEach(vets::add);
        return vets;
    }

    @Override
    public void delete(Vet entity) {
        vetRepo.delete(entity);
    }

    @Override
    public void deleteById(Long id) {
        vetRepo.deleteById(id);
    }
}
