package com.cubisoft.petclinic.jpa;

import com.cubisoft.petclinic.model.VetSpecialty;
import com.cubisoft.petclinic.repo.VetSpecialtyRepo;
import com.cubisoft.petclinic.service.VetSpecialtyService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */
@Service
@Profile("jpa")
public class VetSpecialtyJpa implements VetSpecialtyService {

    private final VetSpecialtyRepo specialtyRepo;

    public VetSpecialtyJpa(VetSpecialtyRepo specialtyRepo) {
        this.specialtyRepo = specialtyRepo;
    }

    @Override
    public VetSpecialty findById(Long id) {
        return specialtyRepo.findById(id).orElse(null);
    }

    @Override
    public VetSpecialty save(VetSpecialty entity) {
        return specialtyRepo.save(entity);
    }

    @Override
    public Set<VetSpecialty> findAll() {
        Set<VetSpecialty> specialties = new HashSet<>();
        specialtyRepo.findAll().forEach(specialties::add);
        return specialties;
    }

    @Override
    public void delete(VetSpecialty entity) {
        specialtyRepo.delete(entity);
    }

    @Override
    public void deleteById(Long id) {
        specialtyRepo.deleteById(id);
    }
}
