package com.cubisoft.petclinic.jpa;

import com.cubisoft.petclinic.model.Visit;
import com.cubisoft.petclinic.repo.VisitRepo;
import com.cubisoft.petclinic.service.VisitService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */
@Service
@Profile("jpa")
public class VisitJpa implements VisitService {

    private final VisitRepo visitRepo;

    public VisitJpa(VisitRepo visitRepo) {
        this.visitRepo = visitRepo;
    }

    @Override
    public Visit findById(Long id) {
        return visitRepo.findById(id).orElse(null);
    }

    @Override
    public Visit save(Visit entity) {
        return visitRepo.save(entity);
    }

    @Override
    public Set<Visit> findAll() {
        Set<Visit> visits = new HashSet<>();
        visitRepo.findAll().forEach(visits::add);
        return visits;
    }

    @Override
    public void delete(Visit entity) {
        visitRepo.delete(entity);
    }

    @Override
    public void deleteById(Long id) {
        visitRepo.deleteById(id);
    }
}
