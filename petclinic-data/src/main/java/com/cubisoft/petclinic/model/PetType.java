package com.cubisoft.petclinic.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */
@Setter
@Getter
@AllArgsConstructor
@Entity
@Table(name = "type")
public class PetType extends NamedEntity {

    @Builder
    public PetType(Long id, String name) {
        super(id);
        this.setName(name);
    }
}
