package com.cubisoft.petclinic.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "vet")
public class Vet extends Person {

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "vet_specialties",
    joinColumns = @JoinColumn(name = "vet_id"),
    inverseJoinColumns = @JoinColumn(name = "specialty_id"))
    private Set<VetSpecialty> vetSpecialties;

    public Set<VetSpecialty> getVetSpecialties() {
        if(vetSpecialties == null)
            vetSpecialties = new HashSet<>();
        return vetSpecialties;
    }

    public void setVetSpecialties(Set<VetSpecialty> vetSpecialties) {
        this.vetSpecialties = vetSpecialties;
    }
}
