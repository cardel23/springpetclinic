package com.cubisoft.petclinic.repo;

import com.cubisoft.petclinic.model.Owner;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */
public interface OwnerRepo extends CrudRepository<Owner, Long> {

    Owner findByLastName(String lastName);

    /**
     * Under the hood this is what happens.
     *
     * At runtime, Spring Data JPA sees that your interface extends an in-build repository interface CrudRepository.
     *
     * Now CrudRepository is a special repository interface of Spring Data JPA.
     *
     * Check its method https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html
     *
     * Note that you have specified your entity name and ID as parameters to CrudRepository.
     *
     * Now the magic happens at runtime. Spring Data JPA will create an implementation for all the methods of CrudRepository or any method that you define in your interface by following conventions (findByLastName(lastName) is following the convention findBy<entity_field_name>(entityfield_name).
     *
     * @param lastName
     * @return list
     */
    List<Owner> findByLastNameLike(String lastName);
}
