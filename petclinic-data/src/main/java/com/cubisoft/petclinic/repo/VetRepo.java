package com.cubisoft.petclinic.repo;

import com.cubisoft.petclinic.model.Vet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */
public interface VetRepo extends CrudRepository<Vet, Long> {
}
