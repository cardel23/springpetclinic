package com.cubisoft.petclinic.repo;

import com.cubisoft.petclinic.model.VetSpecialty;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */
public interface VetSpecialtyRepo extends CrudRepository<VetSpecialty, Long> {
}
