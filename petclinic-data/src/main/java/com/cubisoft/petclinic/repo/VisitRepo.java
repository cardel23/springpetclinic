package com.cubisoft.petclinic.repo;

import com.cubisoft.petclinic.model.Visit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */
public interface VisitRepo extends CrudRepository<Visit, Long> {
}
