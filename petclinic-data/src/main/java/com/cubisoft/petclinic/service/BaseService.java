package com.cubisoft.petclinic.service;

import java.util.Set;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */

public interface BaseService<T, ID> {

    T findById(ID id);
    T save (T entity);
    Set<T> findAll();
    void delete(T entity);
    void deleteById(ID id);
}
