package com.cubisoft.petclinic.service;

import com.cubisoft.petclinic.model.Owner;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */
public interface OwnerService extends BaseService<Owner, Long> {
    Owner findByLastName(String lastName);
    List<Owner> findByLastNameLike(String lastName);
}
