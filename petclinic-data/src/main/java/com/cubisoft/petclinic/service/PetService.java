package com.cubisoft.petclinic.service;

import com.cubisoft.petclinic.model.Pet;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */
public interface PetService extends BaseService<Pet, Long> {
    Pet save(Pet pet);
    Set<Pet> findAll();
}
