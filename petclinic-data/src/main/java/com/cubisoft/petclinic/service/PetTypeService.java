package com.cubisoft.petclinic.service;

import com.cubisoft.petclinic.model.PetType;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */

public interface PetTypeService extends BaseService<PetType, Long> {
}
