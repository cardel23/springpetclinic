package com.cubisoft.petclinic.service;

import com.cubisoft.petclinic.model.Vet;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */
public interface VetService extends BaseService<Vet, Long> {

}
