package com.cubisoft.petclinic.service;

import com.cubisoft.petclinic.model.VetSpecialty;
import com.cubisoft.petclinic.service.BaseService;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */

public interface VetSpecialtyService extends BaseService<VetSpecialty, Long> {

}
