package com.cubisoft.petclinic.service;

import com.cubisoft.petclinic.model.Visit;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */

public interface VisitService  extends BaseService<Visit, Long> {
}
