package com.cubisoft.petclinic.service.map;

import com.cubisoft.petclinic.model.BaseEntity;

import java.util.*;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */

public abstract class AbstractMapService<T extends BaseEntity, ID extends Long> {

    protected Map<Long, T> map = new HashMap<>();

    Set<T> findAll() {
        return new HashSet<>(map.values());
    }

    T findById(ID id) {
        return map.get(id);
    }

    T save(T entity) {
        if(entity != null)
            if(entity.getId() == null)
                entity.setId(getNextId());
        map.put(entity.getId(), entity);
        return entity;
    }

    void deleteById(ID id) {
        map.remove(id);
    }

    void delete(T entity) {
        map.entrySet().removeIf(entry -> entry.getValue().equals(entity));
    }

    private Long getNextId(){
        try {
            return Collections.max(map.keySet())+1;
        } catch (NoSuchElementException e){
            return 1l;
        }
    }
}
