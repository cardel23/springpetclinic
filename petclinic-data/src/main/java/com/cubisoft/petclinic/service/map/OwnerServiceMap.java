package com.cubisoft.petclinic.service.map;

import com.cubisoft.petclinic.model.Owner;
import com.cubisoft.petclinic.model.Pet;
import com.cubisoft.petclinic.service.OwnerService;
import com.cubisoft.petclinic.service.PetService;
import com.cubisoft.petclinic.service.PetTypeService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */
@Service
@Profile({"default","map"})
public class OwnerServiceMap extends AbstractMapService<Owner, Long> implements OwnerService {

    private final PetTypeService petTypeService;
    private final PetService petService;

    public OwnerServiceMap(PetTypeService petTypeService, PetService petService) {
        this.petTypeService = petTypeService;
        this.petService = petService;
    }

    @Override
    public Owner findById(Long id) {
        return super.findById(id);
    }

    @Override
    public Owner save(Owner entity) {
        if(entity != null) {
            if(entity.getPets() != null){
                entity.getPets().forEach(pet->{
                    if(pet.getPetType() != null){
                        if(pet.getPetType().getId() == null)
                            pet.setPetType(petTypeService.save(pet.getPetType()));
                    } else
                        throw new RuntimeException("Error");
                    if(pet.getId() == null){
                        Pet saved = petService.save(pet);
                        pet.setId(saved.getId());
                    }
                });
            }
            return super.save(entity);
        }
        else
            return null;
    }

    @Override
    public Set<Owner> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Owner entity) {
        super.delete(entity);
    }

    @Override
    public Owner findByLastName(String lastName) {
        return this.findAll()
                .stream()
                .filter(owner -> owner.getLastName().equalsIgnoreCase("Porto"))
                .findFirst().orElse(null);
    }

    @Override
    public List<Owner> findByLastNameLike(String lastName) {
        return null;
    }

}
