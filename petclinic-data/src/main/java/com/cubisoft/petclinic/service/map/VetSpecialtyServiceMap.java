package com.cubisoft.petclinic.service.map;

import com.cubisoft.petclinic.model.VetSpecialty;
import com.cubisoft.petclinic.service.VetSpecialtyService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */
@Service
@Profile({"default","map"})
public class VetSpecialtyServiceMap extends AbstractMapService<VetSpecialty, Long> implements VetSpecialtyService {

    @Override
    public VetSpecialty findById(Long id) {
        return super.findById(id);
    }

    @Override
    public VetSpecialty save(VetSpecialty entity) {
        return super.save(entity);
    }

    @Override
    public Set<VetSpecialty> findAll() {
        return super.findAll();
    }

    @Override
    public void delete(VetSpecialty entity) {
        super.delete(entity);
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }
}
