package com.cubisoft.petclinic.service.map;

import com.cubisoft.petclinic.model.Visit;
import com.cubisoft.petclinic.service.VisitService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;

/** 
 * Created by Carlos Delgadillo on 23/11/20
 */
@Service
@Profile({"default","map"})
public class VisitServiceMap extends AbstractMapService<Visit, Long> implements VisitService {
    @Override
    public Visit findById(Long id) {
        return super.findById(id);
    }

    @Override
    public Visit save(Visit entity) {
        if (entity.getPet() == null || entity.getPet().getOwner() == null || entity.getPet().getOwner().getId() == null || entity.getPet().getId() == null)
            throw new RuntimeException("Invalid object");
        return super.save(entity);
    }

    @Override
    public Set<Visit> findAll() {
        return super.findAll();
    }

    @Override
    public void delete(Visit entity) {
        super.delete(entity);
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }
}
