package com.cubisoft.petclinic.jpa;

import com.cubisoft.petclinic.model.Owner;
import com.cubisoft.petclinic.repo.OwnerRepo;
import com.cubisoft.petclinic.repo.PetRepo;
import com.cubisoft.petclinic.repo.PetTypeRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */
@ExtendWith(MockitoExtension.class)
class OwnerJpaTest {

    public static final String LAST_NAME = "Porto";
    @Mock OwnerRepo ownerRepo;
    @Mock PetRepo petRepo;
    @Mock PetTypeRepo petTypeRepo;

    @InjectMocks OwnerJpa ownerJpa;
    @BeforeEach
    void setUp() {
    }

    @Test
    void findByLastName() {
        Owner returnOwner = Owner.builder().id(1l).lastName(LAST_NAME).build();
        when(ownerRepo.findByLastName(any())).thenReturn(returnOwner);
        Owner owner = ownerJpa.findByLastName(LAST_NAME);
        assertEquals(LAST_NAME, owner.getLastName());
        verify(ownerRepo).findByLastName(any());
    }

    @Test
    void findById() {
    }

    @Test
    void save() {
    }

    @Test
    void findAll() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteById() {
    }
}