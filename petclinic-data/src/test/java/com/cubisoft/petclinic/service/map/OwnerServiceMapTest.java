package com.cubisoft.petclinic.service.map;

import com.cubisoft.petclinic.model.Owner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by Carlos Delgadillo on 23/11/20
 */

class OwnerServiceMapTest {

    OwnerServiceMap serviceMap;
    Long ownerId = 1l;
    String lastName = "Porto";
    @BeforeEach
    void setUp() {
        serviceMap = new OwnerServiceMap(new PetTypeServiceMap(), new PetServiceMap());
        serviceMap.save(Owner.builder().id(1l).lastName("Porto").build());
    }

    @Test
    void findById() {
        Owner owner = serviceMap.findById(ownerId);
        assertEquals(ownerId, owner.getId());
    }

    @Test
    void saveExistingId() {
        long id = 2l;
        Owner owner = Owner.builder().id(2l).build();
        Owner saved = serviceMap.save(owner);
        assertEquals(id, saved.getId());

    }

    @Test
    void saveNoId(){
        Owner saved = serviceMap.save(Owner.builder().build());
        assertNotNull(saved);
        assertNotNull(saved.getId());
    }


    @Test
    void findAll() {
        Set<Owner> owners = serviceMap.findAll();
        assertEquals(1, owners.size());
    }

    @Test
    void deleteById() {
        serviceMap.deleteById(ownerId);
        assertEquals(0, serviceMap.findAll().size());
    }

    @Test
    void delete() {
        serviceMap.delete(serviceMap.findById(ownerId));
        assertEquals(0, serviceMap.findAll().size());
    }

    @Test
    void findByLastName() {
        Owner owner = serviceMap.findByLastName(lastName);
        assertNotNull(owner);
        assertEquals(ownerId, owner.getId());
    }
}