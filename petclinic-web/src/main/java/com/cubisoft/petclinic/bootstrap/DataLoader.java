package com.cubisoft.petclinic.bootstrap;

import com.cubisoft.petclinic.model.*;
import com.cubisoft.petclinic.service.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Created by Carlos Delgadillo on 22/11/20
 */

@Component
public class DataLoader implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final PetService petService;
    private final PetTypeService petTypeService;
    private final VetSpecialtyService specialtyService;
    private final VisitService visitService;

    public DataLoader(OwnerService ownerService, VetService vetService, PetService petService, PetTypeService petTypeService, VetSpecialtyService specialtyService, VisitService visitService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petService = petService;
        this.petTypeService = petTypeService;
        this.specialtyService = specialtyService;
        this.visitService = visitService;
    }

    @Override
    public void run(String... args) throws Exception {
        if(petTypeService.findAll().size() == 0)
            loadData();
    }

    private void loadData() {
        System.out.println("DATA LOADER...");

        PetType petType = new PetType();
        petType.setName("Perro");
        PetType savedPetType = petTypeService.save(petType);

        PetType cat = new PetType();
        cat.setName("Gato");
        PetType savedCatType = petTypeService.save(cat);

        Owner owner = new Owner();
        owner.setFirstName("Carlos");
        owner.setLastName("Delgadillo");
        owner.setCity("Masaya");
        owner.setAddress("Nindiri");
        owner.setTelephone("86791256");
        ownerService.save(owner);

        Pet perrito = new Pet();
        perrito.setPetType(savedPetType);
        perrito.setName("Perrito");
        perrito.setBirthDate(LocalDate.now().minusMonths(2));
        perrito.setOwner(owner);
        owner.getPets().add(perrito);
        ownerService.save(owner);

        Owner owner2 = new Owner();
        owner2.setFirstName("Fer");
        owner2.setLastName("Porto");
        owner2.setCity("Masaya");
        owner2.setAddress("Nindiri");
        owner2.setTelephone("86791256");
        ownerService.save(owner2);

        Pet gata = new Pet();
        gata.setPetType(savedCatType);
        gata.setOwner(owner2);
        gata.setBirthDate(LocalDate.now().minusMonths(2));
        gata.setName("Gata");
        owner2.getPets().add(gata);
//        ownerService.save(owner2);

        petService.save(gata);

        Visit visitaGata = new Visit();
        visitaGata.setPet(gata);
        visitaGata.setDescription("Visita de gata");
        visitaGata.setDate(LocalDate.now());

        visitService.save(visitaGata);

        VetSpecialty cirugia = new VetSpecialty();
        cirugia.setName("Cirugia");

        VetSpecialty dentista = new VetSpecialty();
        cirugia.setName("Dentista");

        VetSpecialty radiologia = new VetSpecialty();
        cirugia.setName("Radiologia");

        VetSpecialty savedDen = specialtyService.save(dentista);
        VetSpecialty savedRad = specialtyService.save(radiologia);
        VetSpecialty savedCir = specialtyService.save(cirugia);

        Vet vet = new Vet();
        vet.setFirstName("Michin");
        vet.setLastName("Gorro");
        vet.getVetSpecialties().add(savedCir);
        vetService.save(vet);

        Vet vet2 = new Vet();
        vet2.setFirstName("Pollo");
        vet2.setLastName("Macho");
        vet2.getVetSpecialties().add(radiologia);
        vetService.save(vet2);

        Vet vet3 = new Vet();
        vet3.setFirstName("Fulano");
        vet3.setLastName("Detal");
        vet3.getVetSpecialties().add(dentista);
        vetService.save(vet3);



        System.out.println("Owners o k ase");
    }
}
