package com.cubisoft.petclinic.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */

@Controller
public class IndexController {

    @RequestMapping({"/","","index","index.html"})
    public String index(){
        return "index";
    }

    @RequestMapping("/fail")
    public String errorHandler(){
        return "error";
    }
}
