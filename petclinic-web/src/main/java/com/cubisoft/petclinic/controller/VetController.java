package com.cubisoft.petclinic.controller;

import com.cubisoft.petclinic.service.VetService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Carlos Delgadillo on 21/11/20
 */

@Controller
public class VetController {

    private final VetService vetService;

    public VetController(VetService vetService) {
        this.vetService = vetService;
    }

    @RequestMapping({"/vets","/vets.html","/vets/index", "/vets/index.html"})
    public String list(Model model){
        model.addAttribute("vets", vetService.findAll());
        return "vets/index";
    }
}
